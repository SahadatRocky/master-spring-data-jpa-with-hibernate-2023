package com.springdata.springboot.repository;

import com.springdata.springboot.entity.Product;
import com.springdata.springboot.entity.ProductCategory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.stream.Collectors;

@SpringBootTest
public class ProductCategoryRepositoryTest {

    @Autowired
    private ProductCategoryRepository productCategoryRepository;

    @Test
    void saveProductCategory(){
        ProductCategory productCategory = new ProductCategory();
        productCategory.setCategoryName("books");
        productCategory.setCategoryDescription("books description");

        Product product1 = new Product();
        product1.setName("Core Java");
        product1.setPrice(1000.00);
        product1.setImageUrl("image1.png");
        product1.setSku("ABCD");
        product1.setActive(true);
        product1.setCategory(productCategory);
        productCategory.getProducts().add(product1);

        Product product2 = new Product();
        product2.setName("Effective Java");
        product2.setPrice(2000.00);
        product2.setImageUrl("image2.png");
        product2.setSku("ABCDE");
        product2.setActive(true);
        product2.setCategory(productCategory);
        productCategory.getProducts().add(product2);

        productCategoryRepository.save(productCategory);
    }

    @Test
    void fetchProductCategory(){
        ProductCategory productCategory = productCategoryRepository.findById(1L).get();
        System.out.println(productCategory);

        List<Product> products = productCategory.getProducts().stream().collect(Collectors.toList());
        System.out.println(products);

        products.forEach(System.out::println);

//        for (Product product : productCategory.getProducts()){
//            System.out.println(product);
//        }
    }

}
