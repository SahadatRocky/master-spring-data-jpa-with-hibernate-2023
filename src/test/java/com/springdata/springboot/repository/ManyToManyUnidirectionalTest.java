package com.springdata.springboot.repository;

import com.springdata.springboot.entity.Role;
import com.springdata.springboot.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Set;

@SpringBootTest
public class ManyToManyUnidirectionalTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    void saveUser(){
       User user = new User();
       user.setName("Sahadat");
       user.setEmail("sahadat@gmail.com");
       user.setPassword("123");

       Role admin = new Role();
       admin.setName("Admin");

       Role customer = new Role();
       customer.setName("customer");

       user.getRoles().addAll(Set.of(admin, customer));
       userRepository.save(user);
    }

    @Test
    void fetchUser(){
        User user = userRepository.findById(2L).get();
        System.out.println(user);
        user.getRoles().forEach(System.out::println);
    }

    @Test
    void updateUser(){
        User user = userRepository.findById(2L).get();
        user.setName("Rocky");
        user.setEmail("Rocky@gmail.com");
        user.setPassword("secret");

        Role user_role = new Role();
        user_role.setName("user");

        user.getRoles().clear();

        user.getRoles().add(user_role);

        userRepository.save(user);
    }

    @Test
    void deleteUser(){
        userRepository.deleteById(1L);
    }

}
