package com.springdata.springboot.repository;

import com.springdata.springboot.entity.Product;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.List;

@SpringBootTest
public class ProductRepositoryTest {

    @Autowired
    private ProductRepository productRepository;

    @Test
    void saveMethod(){

        Product product = new Product();
        product.setName("product 1");
        product.setDescription("product 1 description");
        product.setSku("100ABC");
        product.setPrice(100.00);
        product.setActive(true);
        product.setImageUrl("product1.png");

        Product saveObject = productRepository.save(product);

        System.out.println(saveObject.getId());
        System.out.println(saveObject.toString());
    }

    @Test
    void updateUsingSaveMethod(){

        // find or retrieve an entity by id
        Long id = 1l;
        Product product = productRepository.findById(id).get();

        // update entity information
        product.setName("updated product 1");
        product.setDescription("updated product 1 desc");

        // save updated entity
        productRepository.save(product);

    }

    @Test
    void findByIdMethod(){
        Long id = 1L;
        Product product = productRepository.findById(id).get();
        System.out.println(product.toString());
    }

    @Test
    void saveAllMethod(){

        // create product
        Product product = new Product();
        product.setName("product 4");
        product.setDescription("product 4 description");
        product.setSku("444ABCD");
        product.setPrice(400.00);
        product.setActive(true);
        product.setImageUrl("product4.png");

        // create product
        Product product3 = new Product();
        product3.setName("product 5");
        product3.setDescription("product 5 description");
        product3.setSku("555ABCDE");
        product3.setPrice(500.00);
        product3.setActive(true);
        product3.setImageUrl("product5.png");

        productRepository.saveAll(List.of(product, product3));
    }

    @Test
    void findAllMethod(){

        List<Product> products = productRepository.findAll();

        products.forEach(p -> {
            System.out.println(p.getName());
        });
    }

    @Test
    void deleteByIdMethod() {
        Long id = 1L;
        productRepository.deleteById(id);
    }

    @Test
    void deleteMethod(){
        Long id = 2L;
        Product product = productRepository.findById(id).get();
        productRepository.delete(product);
    }

    @Test
    void deleteAllMethod(){
        Long id = 5L;
        Product product1 = productRepository.findById(id).get();

        Long id2 = 6L;
        Product product2 = productRepository.findById(id2).get();

        productRepository.deleteAll(List.of(product1, product2));
    }


    @Test
    void existsByIdMethod(){
        Long id = 2L;
        boolean exist = productRepository.existsById(id);

        System.out.println(exist);
    }

    @Test
    void countMethod(){
        long count = productRepository.count();
        System.out.println(count);
    }


    }
