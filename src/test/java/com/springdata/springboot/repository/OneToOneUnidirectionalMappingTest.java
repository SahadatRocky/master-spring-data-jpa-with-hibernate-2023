package com.springdata.springboot.repository;

import com.springdata.springboot.entity.Address;
import com.springdata.springboot.entity.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class OneToOneUnidirectionalMappingTest {

    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private AddressRepository addressRepository;

    @Test
    void saveOrderMethod(){

        Order order = new Order();
        order.setOrderTrackingNumber("22EA");
        order.setTotalQuantity(15);
        order.setStatus("IN PROGRESS");
        order.setTotalPrice(1000.00);
        order.setShoppingCartId(1L);

        Address address = new Address();
        address.setCity("Dhaka");
        address.setStreet("Sector-13");
        address.setState("Uttara");
        address.setCountry("Bangladesh");
        address.setZipCode("1212");

        order.setBillingAddress(address);
        orderRepository.save(order);
    }

    @Test
    void updateOrderMethod(){
        Order order = orderRepository.findById(2L).get();
        order.setStatus("DELIVERED");
        order.getBillingAddress().setZipCode("1230");

        orderRepository.save(order);
    }

    @Test
    void getOrderMethod(){
        Long id = 1L;
        Order order = orderRepository.findById(id).get();
        System.out.println(order);

        Address address = addressRepository.findById(1L).get();
        System.out.println(address);
        System.out.println(address.getOrder());
    }
    @Test
    void deleteOrderMethod(){
        orderRepository.deleteById(3L);
    }

}
