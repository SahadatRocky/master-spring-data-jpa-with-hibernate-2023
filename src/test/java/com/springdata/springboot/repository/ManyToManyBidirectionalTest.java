package com.springdata.springboot.repository;

import com.springdata.springboot.entity.Role;
import com.springdata.springboot.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Set;

@SpringBootTest
public class ManyToManyBidirectionalTest {

    @Autowired
    private RoleRepository roleRepository;

    @Test
    void saveRole(){
        Role branchAdmin = new Role();
        branchAdmin.setName("Branch_Admin");

        User a1 = new User();
        a1.setName("A1");
        a1.setEmail("a@gmail.com");
        a1.setPassword("a1");
        a1.getRoles().add(branchAdmin);

        User b1 = new User();
        b1.setName("B1");
        b1.setEmail("b@gmail.com");
        b1.setPassword("b1");
        b1.getRoles().add(branchAdmin);

        branchAdmin.getUsers().addAll(Set.of(a1,b1));

        roleRepository.save(branchAdmin);
    }

    @Test
    void FetchRole(){
        Role role = roleRepository.findById(7L).get();
        System.out.println(role);
        System.out.println(role.getUsers().stream().toList());


    }

}
