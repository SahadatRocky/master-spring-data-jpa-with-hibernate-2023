package com.springdata.springboot.repository;

import com.springdata.springboot.entity.Address;
import com.springdata.springboot.entity.Order;
import com.springdata.springboot.entity.OrderItem;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
public class OneToManyMappingTest {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ProductRepository productRepository;

    // save order along with also save it's order items
    @Test
    void saveOrderMethod(){

        Order order = new Order();
        order.setOrderTrackingNumber("A1");
        order.setStatus("In progress");
        order.setShoppingCartId(1L);
        // create Address
        Address address = new Address();
        address.setCity("Dhaka");
        address.setStreet("Sector-13");
        address.setState("Uttara");
        address.setCountry("Bangladesh");
        address.setZipCode("1212");

        order.setBillingAddress(address);

        // create order item 1
        OrderItem orderItem1 = new OrderItem();
        orderItem1.setImageUrl("image1.png");
        orderItem1.setProduct(productRepository.findById(1L).get());
        orderItem1.setQuantity(2);
        orderItem1.setPrice(orderItem1.getProduct().getPrice() * orderItem1.getQuantity());
        orderItem1.setOrder(order);
        order.getOrderItems().add(orderItem1);

        // create order item 2
        OrderItem orderItem2 = new OrderItem();
        orderItem2.setProduct(productRepository.findById(2L).get());
        orderItem2.setQuantity(3);
        orderItem2.setPrice(orderItem2.getProduct().getPrice() * orderItem2.getQuantity()); // Corrected this line
        orderItem2.setImageUrl("image2.png");
        orderItem2.setOrder(order);
        order.getOrderItems().add(orderItem2);

        order.setTotalQuantity(order.getOrderItems().stream().mapToInt(OrderItem::getQuantity).sum());
        Double price = order.getOrderItems().stream().mapToDouble(OrderItem::getPrice).sum();
        order.setTotalPrice(price);

        orderRepository.save(order);
    }

    @Test
    void fetchOrderMethod(){
        Order order = orderRepository.findById(2L).get();
        System.out.println(order.getStatus());
        System.out.println(order);
        System.out.println(order.getOrderItems());
        for(OrderItem item : order.getOrderItems()){
            System.out.println(item.getProduct().getName());
        }
    }

    @Test
    void deleteOrderMethod(){
        orderRepository.deleteById(2L);
    }

}
