{
    "order":{
        "totalQuantity": 3,
        "shoppingCartId": 2,
        "totalPrice": 2000,
        "status": "IN Progress",

        "billingAddress":{
            "city": "Dhaka",
            "street": "Sector-13",
            "state": "Uttara",
            "country": "Bangladesh",
            "zipCode": "1230"
        },
        "orderItems":[{
                "imageUrl" : "image3.png",
                "quantity": 2,
                "product":{
                    "id": 4
                }
            }
        ]
    },
    "payment":{
        "type": "DEBIT",
        "cardName": "mr. x",
        "cardNumber": "1234 1234 1234",
        "expireMonth": 2,
        "expireYear": 2025,
        "cvc" : 123
    }
}