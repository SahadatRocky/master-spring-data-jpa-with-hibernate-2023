package com.springdata.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDataJpaCourceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringDataJpaCourceApplication.class, args);
	}

}
