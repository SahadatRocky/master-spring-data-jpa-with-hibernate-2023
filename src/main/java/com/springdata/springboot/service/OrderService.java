package com.springdata.springboot.service;

import com.springdata.springboot.dto.OrderRequest;
import com.springdata.springboot.dto.OrderResponse;
import com.springdata.springboot.entity.Order;
import com.springdata.springboot.entity.OrderItem;
import com.springdata.springboot.entity.Payment;
import com.springdata.springboot.exception.PaymentException;
import com.springdata.springboot.repository.OrderRepository;
import com.springdata.springboot.repository.PaymentRepository;
import com.springdata.springboot.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
public class OrderService {

    private OrderRepository orderRepository;
    private PaymentRepository paymentRepository;

    private ProductRepository productRepository;

    public OrderService(OrderRepository orderRepository, PaymentRepository paymentRepository,ProductRepository productRepository) {
        this.orderRepository = orderRepository;
        this.paymentRepository = paymentRepository;
        this.productRepository = productRepository;
    }


    @Transactional
    public OrderResponse placeOrder(OrderRequest orderRequest) {

        // Create a new Order and set its attributes
        Order order = new Order();
        order.setOrderTrackingNumber(UUID.randomUUID().toString());
        order.setBillingAddress(orderRequest.getOrder().getBillingAddress());
        order.setStatus(orderRequest.getOrder().getStatus());
        order.setShoppingCartId(orderRequest.getOrder().getShoppingCartId());
        // Process each order item
        orderRequest.getOrder().getOrderItems().forEach(itemRequest -> {
            OrderItem orderItem = new OrderItem();
            orderItem.setImageUrl(itemRequest.getImageUrl());

            // Assuming you have a valid product ID, use productRepository to fetch the product
            // Adjust the findById parameter based on your actual product ID
            orderItem.setProduct(productRepository.findById(itemRequest.getProduct().getId()).orElse(null));

            orderItem.setQuantity(itemRequest.getQuantity());
            orderItem.setPrice(orderItem.getProduct().getPrice() * orderItem.getQuantity());
            orderItem.setOrder(order);

            order.getOrderItems().add(orderItem);
        });

        // Calculate total quantity and total price for the order
        order.setTotalQuantity(order.getOrderItems().stream().mapToInt(OrderItem::getQuantity).sum());
        double totalPrice = order.getOrderItems().stream().mapToDouble(OrderItem::getPrice).sum();
        order.setTotalPrice(totalPrice);

        // Save the order to the database
        orderRepository.save(order);

        // Process payment
        processPayment(orderRequest.getPayment(), order.getId());

        // Create and return the order response
        OrderResponse orderResponse = new OrderResponse();
        orderResponse.setOrderTrackingNumber(order.getOrderTrackingNumber());
        orderResponse.setStatus(order.getStatus());
        orderResponse.setMessage("SUCCESS");
        return orderResponse;
    }

    private void processPayment(Payment payment, Long orderId) {
        if (!payment.getType().equals("DEBIT")) {
            throw new PaymentException("Payment card type is not supported.");
        }

        // Set the order ID for the payment and save it to the database
        payment.setOrderId(orderId);
        paymentRepository.save(payment);
    }

}
