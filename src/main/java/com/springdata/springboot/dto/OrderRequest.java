package com.springdata.springboot.dto;

import com.springdata.springboot.entity.Order;
import com.springdata.springboot.entity.Payment;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OrderRequest {
    private Order order;
    private Payment payment;
}
